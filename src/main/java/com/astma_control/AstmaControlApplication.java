package com.astma_control;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AstmaControlApplication {

    public static void main(String[] args) {
        SpringApplication.run(AstmaControlApplication.class, args);
    }

}
