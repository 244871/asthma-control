package com.astma_control.Controllers;

import com.astma_control.Models.Zazucie;
import com.astma_control.DTOs.NewZazucieDTO;
import com.astma_control.DTOs.ZazucieDTO;
import com.astma_control.Services.ZazucieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Stream;

@RestController
@RequestMapping("/zazucia")
public class ZazuciaController {

    @Autowired
    private ZazucieService zazucieService;

    @PostMapping("/add")
    public Zazucie add(@RequestBody NewZazucieDTO newZazucieDTO){
        return zazucieService.add(newZazucieDTO);
    }

    @GetMapping("/forDays")
    public Stream<ZazucieDTO> allForPacjent(@RequestParam String login, @RequestParam int dni){
        return zazucieService.allForPacjent(login, dni);
    }

    @GetMapping("/today")
    public Stream<ZazucieDTO> allForPacjentToday(@RequestParam String login){
        return zazucieService.allForPacjentToday(login);
    }
}
