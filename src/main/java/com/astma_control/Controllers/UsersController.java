package com.astma_control.Controllers;


import com.astma_control.Models.Lekarz;
import com.astma_control.Models.Pacjent;
import com.astma_control.DTOs.*;
import com.astma_control.Services.UsersServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersServices usersServices;

    @PostMapping("/add")
    public Pacjent add(@RequestBody NewPacjentDTO newPacjent_DTO){
        return usersServices.add(newPacjent_DTO);
    }

    @PostMapping("/addL")
    public Lekarz add(@RequestBody LogPassDTO newLekarzDTO){
        return usersServices.addL(newLekarzDTO);
    }
    
    @GetMapping("/getDoctors")
    public ArrayList<LogDTO> getAll(){
        return usersServices.getAll();
    }

    @GetMapping("/getPacjentsForDoctor")
    public ArrayList<LogDTO> getPacjents(@RequestParam String lekarz_login){
        return usersServices.getPacjents(lekarz_login);
    }

    @PutMapping("/modify/password")
    public Pacjent modifyPassword(@RequestBody LogPassDTO pacjent_DTO){
        return usersServices.modifyPassword(pacjent_DTO);
    }

    @PutMapping("/modify/doctor")
    public Pacjent modifyDoctor(@RequestBody ModifyDocDTO modifyDocDTO){
        return usersServices.modifyDoctor(modifyDocDTO);
    }
}
