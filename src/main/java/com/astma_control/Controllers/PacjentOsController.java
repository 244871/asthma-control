package com.astma_control.Controllers;

import com.astma_control.Models.Lek_os;
import com.astma_control.DTOs.POsDTO;
import com.astma_control.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/pacjentos")
public class PacjentOsController {

    @Autowired
    private POsService pOsService;


    @PostMapping("/modify")  //add wygląda dokładnie tak samo, więc nie ma sensu ich rozdzielać
    private Lek_os modify(@RequestBody POsDTO pOsDTO){
        return pOsService.modify(pOsDTO); //zapisuje lek os w bazie oraz przypisuje go pacjentowi
    }

}
