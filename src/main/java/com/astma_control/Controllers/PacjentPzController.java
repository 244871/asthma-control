package com.astma_control.Controllers;

import com.astma_control.DTOs.GodzinyDTO;
import com.astma_control.Models.Lek_pz;
import com.astma_control.DTOs.PPzDTO;
import com.astma_control.Services.PPzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pacjentpz")
public class PacjentPzController {

    @Autowired
    private PPzService pPzService;


    @PostMapping("/modify")  //add wygląda dokładnie tak samo, więc nie ma sensu ich rozdzielać
    private Lek_pz modify(@RequestBody PPzDTO pPzDTO){
        return pPzService.modify(pPzDTO); //zapisuje lek pz w bazie oraz przypisuje go pacjentowi
    }

    @GetMapping("/getGodziny")
    private GodzinyDTO getGodziny(@RequestParam String login){
        return pPzService.getGodzinyForUser(login);
    }
}
