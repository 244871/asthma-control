package com.astma_control.Controllers;

import com.astma_control.Services.AuthorisationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authorisation")
public class AutorisationController {

    @Autowired
    private AuthorisationService authorisationService;


    @GetMapping("/login/exist")
    public Boolean loginExistence(@RequestParam String login){
        return authorisationService.loginExist(login);
    }

    @GetMapping("/verify")
    public Boolean verify(@RequestParam String login, @RequestParam String password){
        return authorisationService.verify(login, password);
    }

    @GetMapping("/doctor")
    public Boolean amIaDoctor(@RequestParam String login){
        return authorisationService.amIaDoctor(login);
    }
}
