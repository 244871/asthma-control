package com.astma_control.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalTime;

@Entity
public class Lek_pz {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nazwa;
    private int dawki_rano;
    private int dawki_noc;
    private LocalTime godz_rano;
    private LocalTime godz_noc;

    public Lek_pz() {
    }

    public Lek_pz(String nazwa, int dawki_rano, int dawki_noc, LocalTime godz_rano, LocalTime godz_noc) {
        this.nazwa = nazwa;
        this.dawki_rano = dawki_rano;
        this.dawki_noc = dawki_noc;
        this.godz_rano = godz_rano;
        this.godz_noc = godz_noc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getDawki_rano() {
        return dawki_rano;
    }

    public void setDawki_rano(int dawki_rano) {
        this.dawki_rano = dawki_rano;
    }

    public int getDawki_noc() {
        return dawki_noc;
    }

    public void setDawki_noc(int dawki_noc) {
        this.dawki_noc = dawki_noc;
    }

    public LocalTime getGodz_rano() {
        return godz_rano;
    }

    public void setGodz_rano(LocalTime godz_rano) {
        this.godz_rano = godz_rano;
    }

    public LocalTime getGodz_noc() {
        return godz_noc;
    }

    public void setGodz_noc(LocalTime godz_noc) {
        this.godz_noc = godz_noc;
    }
}
