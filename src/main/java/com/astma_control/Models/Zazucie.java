package com.astma_control.Models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Zazucie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDate date;
    private int dawka;

    @ManyToOne
    private Lek_os lek_os;
    @ManyToOne
    private Lek_pz lek_pz;
    @ManyToOne
    private Pacjent pacjent;

    public Zazucie(LocalDate date, int dawka, Lek_os lek_os, Lek_pz lek_pz, Pacjent pacjent) {
        this.date = date;
        this.dawka = dawka;
        this.lek_os = lek_os;
        this.lek_pz = lek_pz;
        this.pacjent = pacjent;
    }

    public Zazucie(LocalDate date, int dawka, Pacjent pacjent) {
        this.date = date;
        this.dawka = dawka;
        this.pacjent = pacjent;
    }

    public Zazucie() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getDawka() {
        return dawka;
    }

    public void setDawka(int dawka) {
        this.dawka = dawka;
    }

    public Lek_os getLek_os() {
        return lek_os;
    }

    public void setLek_os(Lek_os lek_os) {
        this.lek_os = lek_os;
    }

    public Lek_pz getLek_pz() {
        return lek_pz;
    }

    public void setLek_pz(Lek_pz lek_pz) {
        this.lek_pz = lek_pz;
    }

    public Pacjent getPacjent() {
        return pacjent;
    }

    public void setPacjent(Pacjent pacjent) {
        this.pacjent = pacjent;
    }
}
