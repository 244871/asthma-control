package com.astma_control.Models;

import javax.persistence.*;

@Entity
public class Pacjent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String login;
    private String password;

    @ManyToOne
    private Lekarz lekarz;
    @OneToOne
    private Lek_os lek_os;
    @OneToOne
    private Lek_pz lek_pz;

    public Pacjent() {
    }

    public Pacjent(String login, String password, Lekarz lekarz) {
        this.login = login;
        this.password = password;
        this.lekarz = lekarz;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Lekarz getLekarz() {
        return lekarz;
    }

    public void setLekarz(Lekarz lekarz) {
        this.lekarz = lekarz;
    }

    public Lek_os getLek_os() {
        return lek_os;
    }

    public void setLek_os(Lek_os lek_os) {
        this.lek_os = lek_os;
    }

    public Lek_pz getLek_pz() {
        return lek_pz;
    }

    public void setLek_pz(Lek_pz lek_pz) {
        this.lek_pz = lek_pz;
    }
}
