package com.astma_control.Repos;

import com.astma_control.Models.Lek_os;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Lek_osRepo extends CrudRepository<Lek_os, LayerInstantiationException> {
}
