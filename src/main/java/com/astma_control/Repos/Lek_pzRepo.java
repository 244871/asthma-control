package com.astma_control.Repos;

import com.astma_control.Models.Lek_pz;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Lek_pzRepo extends CrudRepository<Lek_pz, LayerInstantiationException> {
}
