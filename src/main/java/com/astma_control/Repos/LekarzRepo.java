package com.astma_control.Repos;

import com.astma_control.Models.Lekarz;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LekarzRepo extends CrudRepository<Lekarz, LayerInstantiationException> {
    boolean existsLekarzByLogin (String login);
    boolean existsLekarzByLoginAndPassword (String login, String password);
    Lekarz findLekarzByLogin (String login);
}
