package com.astma_control.Repos;

import com.astma_control.Models.Pacjent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

import static javafx.scene.input.KeyCode.T;

@Repository
public interface PacjentRepo extends CrudRepository<Pacjent, LayerInstantiationException> {
    boolean existsPacjentByLogin (String login);
    boolean existsPacjentByLoginAndPassword (String login, String password);
    Pacjent findPacjentByLogin (String login);
    ArrayList <Pacjent> findPacjentsByLekarz_Login (String lekarz_login);
}
