package com.astma_control.Repos;

import com.astma_control.Models.Zazucie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;

@Repository
public interface ZazucieRepo extends CrudRepository<Zazucie, LayerInstantiationException> {
    boolean existsZazucieByDateAndPacjentLogin (LocalDate date, String login);
    Zazucie findZazucieByDateAndPacjentLogin (LocalDate date, String login);
    ArrayList <Zazucie> findZazuciesByPacjent_Login (String login);
}
