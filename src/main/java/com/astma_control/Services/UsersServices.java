package com.astma_control.Services;

import com.astma_control.Models.Lekarz;
import com.astma_control.Models.Pacjent;
import com.astma_control.DTOs.*;
import com.astma_control.Repos.LekarzRepo;
import com.astma_control.Repos.PacjentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UsersServices {
    private LekarzRepo lekarzRepo;
    private PacjentRepo pacjentRepo;

    @Autowired
    public UsersServices(LekarzRepo lekarzRepo, PacjentRepo pacjentRepo) {
        this.lekarzRepo = lekarzRepo;
        this.pacjentRepo = pacjentRepo;
    }


    //tworzymy nowego pacjenta z null w miejscu leków
    public Pacjent add(NewPacjentDTO newPacjent_DTO){
        Lekarz lekarz = lekarzRepo.findLekarzByLogin(newPacjent_DTO.getLekarz_login());
        if(pacjentRepo.existsPacjentByLogin(newPacjent_DTO.getLogPassDTO().getLogin())){
            return null;
        }else{
            Pacjent pacjent = new Pacjent(newPacjent_DTO.getLogPassDTO().getLogin(), newPacjent_DTO.getLogPassDTO().getPassword(), lekarz);
            return pacjentRepo.save(pacjent);
        }
    }

    //tworzymy nowego lekarza
    public Lekarz addL(LogPassDTO newLekarzDTO){
        if (lekarzRepo.existsLekarzByLogin(newLekarzDTO.getLogin())) {//taki lekarz już istnieje
            return null;
        }else{
            Lekarz lekarz = new Lekarz(newLekarzDTO.getLogin(), newLekarzDTO.getPassword());
            return lekarzRepo.save(lekarz);
        }
    }

    //dostajemy spis wszystkich lekarzy
    public ArrayList<LogDTO> getAll(){
        ArrayList<Lekarz> lekarzs = (ArrayList<Lekarz>) lekarzRepo.findAll();
        ArrayList<LogDTO> lekarz = new ArrayList<>();
        for(int i =0; i<lekarzs.size(); i++){
            lekarz.add(new LogDTO(lekarzs.get(i).getLogin()));
        }
        return lekarz;
    }

    //dostajemy pacjentów danego lekarza
    public ArrayList<LogDTO> getPacjents(String lekarz_login){
        ArrayList<Pacjent> pacjentArrayList = pacjentRepo.findPacjentsByLekarz_Login(lekarz_login);
        ArrayList<LogDTO> pacjentDTOS = new ArrayList<>();
        for(int i=0; i<pacjentArrayList.size(); i++){
            pacjentDTOS.add(new LogDTO(pacjentArrayList.get(i).getLogin()));
        }
        return pacjentDTOS;
    }

    //zmieniamy hasło danego użytkownika
    public Pacjent modifyPassword(LogPassDTO pacjent_DTO) {
        Pacjent pacjent = pacjentRepo.findPacjentByLogin(pacjent_DTO.getLogin());
        if (pacjent != null) {
            pacjent.setPassword(pacjent_DTO.getPassword());
            return pacjentRepo.save(pacjent);
        } else return null;
    }


    //zmieniamy hasło danego użytkownika
    public Pacjent modifyDoctor(ModifyDocDTO modifyDocDTO) {
        Pacjent pacjent = pacjentRepo.findPacjentByLogin(modifyDocDTO.getLogin());
        Lekarz lekarz = lekarzRepo.findLekarzByLogin(modifyDocDTO.getLogin_lekarz());
        if (pacjent != null && lekarz != null) {
            pacjent.setLekarz(lekarz);
            return pacjentRepo.save(pacjent);
        } else return null;
    }
}
