package com.astma_control.Services;

import com.astma_control.DTOs.GodzinyDTO;
import com.astma_control.Models.Lek_pz;
import com.astma_control.Models.Pacjent;
import com.astma_control.DTOs.PPzDTO;
import com.astma_control.Repos.Lek_pzRepo;
import com.astma_control.Repos.PacjentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Service
public class PPzService {

    private Lek_pzRepo lek_pzRepo;
    private PacjentRepo pacjentRepo;

    @Autowired
    public PPzService(Lek_pzRepo lek_pzRepo, PacjentRepo pacjentRepo) {
        this.lek_pzRepo = lek_pzRepo;
        this.pacjentRepo = pacjentRepo;
    }

    DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME;

    //przypisujemy nowy lek pz
    public Lek_pz modify(PPzDTO pPzDTO) {
        Lek_pz lek_pz = new Lek_pz(pPzDTO.getNazwa(), pPzDTO.getDawki_rano(),
                pPzDTO.getDawki_noc(), LocalTime.parse(pPzDTO.getGodz_rano(), formatter), LocalTime.parse(pPzDTO.getGodz_noc(), formatter)); //tworzymyy nowy lek pz na podstawie danych dostarczonych w modelu
        Pacjent pacjent = pacjentRepo.findPacjentByLogin(pPzDTO.getLogin()); //znajdujemy naszego pacjenta
        if (pacjent != null) {
            pacjent.setLek_pz(lek_pz); //ustawiamy pacjentowi jego lek
            return lek_pzRepo.save(lek_pz); //zapisujemy lek pz w bazie
        }
        return null;
    }

    //dostajemy godziny zażywania leków pz
    public GodzinyDTO getGodzinyForUser(String login) {
        Pacjent pacjent = pacjentRepo.findPacjentByLogin(login);
        if (pacjent != null) {
            return new GodzinyDTO(pacjent.getLek_pz().getGodz_rano().format(formatter),
                    pacjent.getLek_pz().getGodz_noc().format(formatter));
        } else return null;
    }
}

