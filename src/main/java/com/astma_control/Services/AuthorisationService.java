package com.astma_control.Services;

import com.astma_control.Repos.LekarzRepo;
import com.astma_control.Repos.PacjentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorisationService {

    private LekarzRepo lekarzRepo;
    private PacjentRepo pacjentRepo;

    @Autowired
    public AuthorisationService(LekarzRepo lekarzRepo, PacjentRepo pacjentRepo) {
        this.lekarzRepo = lekarzRepo;
        this.pacjentRepo = pacjentRepo;
    }


    //sprawdzamy czy login już istnieje
    public Boolean loginExist(String login){
        return (lekarzRepo.existsLekarzByLogin(login)||pacjentRepo.existsPacjentByLogin(login));
    }

    //sprawdzamy czy hasło pasuje do loginu
    public Boolean verify(String login, String password){
        return (pacjentRepo.existsPacjentByLoginAndPassword(login, password)||
                lekarzRepo.existsLekarzByLoginAndPassword(login, password));
    }

    //sprawdzamy czy zalogowana osoba jest lekarzem
    public Boolean amIaDoctor(String login){
        return lekarzRepo.existsLekarzByLogin(login);
    }
}
