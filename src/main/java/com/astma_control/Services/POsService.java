package com.astma_control.Services;

import com.astma_control.Models.Lek_os;
import com.astma_control.Models.Pacjent;
import com.astma_control.DTOs.POsDTO;
import com.astma_control.Repos.Lek_osRepo;
import com.astma_control.Repos.PacjentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class POsService {

    private Lek_osRepo lek_osRepo;
    private PacjentRepo pacjentRepo;

    @Autowired
    public POsService(Lek_osRepo lek_osRepo, PacjentRepo pacjentRepo) {
        this.lek_osRepo = lek_osRepo;
        this.pacjentRepo = pacjentRepo;
    }


    //przypisujemy nowy lek os
    public Lek_os modify(POsDTO pOsDTO){
        Lek_os lek_os = new Lek_os(pOsDTO.getNazwa()); //tworzymyy nowy lek os na podstawie danych dostarczonych w modelu
        Pacjent pacjent = pacjentRepo.findPacjentByLogin(pOsDTO.getLogin()); //znajdujemy naszego pacjenta
        if(pacjent!=null) {
            pacjent.setLek_os(lek_os); //ustawiamy pacjentowi jego lek
            return lek_osRepo.save(lek_os); //zapisujemy lek os w bazie
        }
        return null;
    }
}
