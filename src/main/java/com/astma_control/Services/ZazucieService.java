package com.astma_control.Services;

import com.astma_control.Models.Pacjent;
import com.astma_control.Models.Zazucie;
import com.astma_control.DTOs.NewZazucieDTO;
import com.astma_control.DTOs.ZazucieDTO;
import com.astma_control.Repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class ZazucieService {

    private ZazucieRepo zazucieRepo;
    private PacjentRepo pacjentRepo;

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Autowired
    public ZazucieService(ZazucieRepo zazucieRepo, PacjentRepo pacjentRepo) {
        this.zazucieRepo = zazucieRepo;
        this.pacjentRepo = pacjentRepo;
    }

    // dodawanie nowego zażycia. If else, gdyż jakieś zażycie danego dnia może już istnieć, wtedy robimy put
    public Zazucie add(NewZazucieDTO newZazucieDTO) {
        Zazucie firstZazucieToday = zazucieRepo.findZazucieByDateAndPacjentLogin(LocalDate.parse(newZazucieDTO.getDate(), dateTimeFormatter), newZazucieDTO.getPacjent_login());
        if (firstZazucieToday != null) {
            firstZazucieToday.setDawka(newZazucieDTO.getDawka() + firstZazucieToday.getDawka());
            return zazucieRepo.save(firstZazucieToday);
        } else {
            Pacjent pacjent = pacjentRepo.findPacjentByLogin(newZazucieDTO.getPacjent_login());
            if(pacjent !=null) {
                Zazucie zazucie = new Zazucie(LocalDate.parse(newZazucieDTO.getDate(), dateTimeFormatter), newZazucieDTO.getDawka(), pacjent.getLek_os(), pacjent.getLek_pz(), pacjent);
                return zazucieRepo.save(zazucie);
            }else return null;
        }
    }

    //znajduje zażycia leku dla danego pacjęta w danym przedziale czasu
    public Stream<ZazucieDTO> allForPacjent(String login, int dni){

        ArrayList<Zazucie> zazucia = zazucieRepo.findZazuciesByPacjent_Login(login);
        ArrayList<ZazucieDTO> zazuciaByDay = new ArrayList<>();

        for(Zazucie zazucie: zazucia) { //uzupełniamy zazuciaByDay zazuciami
            zazuciaByDay.add(new ZazucieDTO(zazucie.getDate().format(dateTimeFormatter), zazucie.getDawka(), zazucie.getPacjent().getLogin(),
                    zazucie.getLek_pz().getNazwa(), zazucie.getLek_os().getNazwa()));
        }
        Collections.reverse(zazuciaByDay);
        ArrayList<ZazucieDTO> inOrderZazucia = fillLazyDays(zazuciaByDay, login, dni);
        return inOrderZazucia.stream();
    }

    //uzupełnia metodę wyżej o dni z wynikiem 0
    public ArrayList<ZazucieDTO> fillLazyDays(ArrayList<ZazucieDTO> zazucieByDay, String login, int dni){

        ArrayList<ZazucieDTO> inOrderZazucia = new ArrayList<>();
        Optional<ZazucieDTO> temporal;
        for(int i =0; i<dni; i++){
            int finalI = i;
            temporal =  zazucieByDay.stream()
                    .filter(zazucieDTO -> zazucieDTO.getDate().equals(LocalDate.now().minusDays(finalI).format(dateTimeFormatter)))
                    .findFirst();
            if(temporal.isPresent()) {
                inOrderZazucia.add(temporal.get());
            }else{
                inOrderZazucia.add(new ZazucieDTO(LocalDate.now().minusDays(i).format(dateTimeFormatter), 0, login));
            }
        }
        return inOrderZazucia;
    }

    //zażycia danego pacjenta dzisiaj
    public Stream<ZazucieDTO> allForPacjentToday(String login){
        return allForPacjent(login, 1);
    }

}
