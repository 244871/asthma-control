package com.astma_control.DTOs;

public class PacjentCreate {
    private NewPacjentDTO newPacjentDTO;
    private Lek_osDTO lek_os_DTO;
    private Lek_pzDTO lek_pz_DTO;

    public PacjentCreate(NewPacjentDTO newPacjentDTO, Lek_osDTO lek_os_DTO, Lek_pzDTO lek_pz_DTO) {
        this.newPacjentDTO = newPacjentDTO;
        this.lek_os_DTO = lek_os_DTO;
        this.lek_pz_DTO = lek_pz_DTO;
    }

    public NewPacjentDTO getNewPacjentDTO() {
        return newPacjentDTO;
    }

    public void setNewPacjentDTO(NewPacjentDTO newPacjentDTO) {
        this.newPacjentDTO = newPacjentDTO;
    }

    public Lek_osDTO getLek_os_DTO() {
        return lek_os_DTO;
    }

    public void setLek_os_DTO(Lek_osDTO lek_os_DTO) {
        this.lek_os_DTO = lek_os_DTO;
    }

    public Lek_pzDTO getLek_pz_DTO() {
        return lek_pz_DTO;
    }

    public void setLek_pz_DTO(Lek_pzDTO lek_pz_DTO) {
        this.lek_pz_DTO = lek_pz_DTO;
    }
}
